# Projet fil rouge Gitlab CI

## Modules Python à installer

pip install flake8==5.0.4
pip install pytest==7.2.0
pip install pytest-cov==4.0.0
pip install mkdocs-material==8.5.11

## Lint Dockerfile

```sh
docker run --rm -i hadolint/hadolint < Dockerfile
echo "##Dockerfile.api" >> docs/docker/docker.md && docker run --rm -i --env-file $PWD/hadolint.env hadolint/hadolint < Dockerfile.api | while read line; do echo ${line}"  " >> docs/docker/docker.md; done
```

## ENVSUBST

```sh
envsubst '${IMAGE_TAG_API},${IMAGE_TAG_FRONT}' < docker-compose.yml.template > docker-compose.yml
```

## RUN Test with Flake8

```sh
flake8 api wsgi.py  
flake8 wsgi.py api --format=html --htmldir=lint-report # lint report (flake8-html)
```

## RUN Pytest

```sh
#python -m pytest  
pytest --cov=. --cov-report html:coverage-report #Coverage test
pytest tests/ --html-report=./unit-report/index.html --title='UNIT TEST' #Unit test (pytest-html-reporter)
```

## MKDOCS

```sh
mkdocs new .  
pytest --cov=. | while read line; do echo ${line}$"  " >> docs/converage.md; done
pytest tests/ | while read line; do echo ${line}$"  " >> docs/unit.md; done
flake8 api wsgi.py | while read line; do echo ${line}$"  " >> docs/lint.md; done
./report.sh
mkdocs build -d public
```

## Build venv job

```sh
build-python:
  stage: test
  script:
    - python -m venv venv
    - source venv/bin/activate
    - pip install -r vapormap/app/requirements/production.txt
  artifacts:
    paths:
      - venv
    expire_in: 1 day
```
